import Screenshot from './ui/Screenshot';
import FeedbackButton from './ui/FeedbackMain';

import { ScreenProvider } from './contexts/screenshotContext';

export default function App() {
  return (
    <ScreenProvider>
      <Screenshot />
      <FeedbackButton />
    </ScreenProvider>
  );
}
