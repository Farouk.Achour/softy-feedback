import { Button } from 'antd';
import { uploadFiles } from '../services/apiFiles';
import { supabaseUrl } from '../services/supabase';
import { GoDeviceCameraVideo } from 'react-icons/go';

function ScreenRecording({ setScreenFiles, screenFiles, setModalOpen }: any) {
  async function screenRecording() {
    setModalOpen(false);
    async function recordScreen() {
      return await navigator.mediaDevices.getDisplayMedia({
        audio: true,
        video: true,
        // preferCurrentTab: true,
      } as any);
    }

    async function createRecorder(stream: any, _mimeType: any) {
      // the stream data is stored in this array
      let recordedChunks: any = [];
      console.log('we are recording the stream');

      const mediaRecorder = new MediaRecorder(stream);

      mediaRecorder.ondataavailable = function (e) {
        if (e.data.size > 0) {
          recordedChunks.push(e.data);
          console.log('Making the file bigger');
        }
      };
      mediaRecorder.onstop = async function () {
        console.log('file will be saved');
        await saveFile(recordedChunks);
        recordedChunks = [];
      };
      mediaRecorder.start(200); // For every 200ms the stream data will be stored in a separate chunk.
    }

    async function saveFile(recordedChunks: any) {
      setModalOpen(true);

      let blob = new Blob(recordedChunks, {
        type: 'video/mp4',
      });
      let filename = new Date().getTime();

      var file = new File([blob], `${filename}.mp4`, {
        type: 'video/mp4',
      });
      await uploadFiles(file);
      setScreenFiles([
        ...screenFiles,
        `${supabaseUrl}/storage/v1/object/public/files/${filename}.mp4`,
      ]);
    }

    let stream = await recordScreen();
    console.log(stream);
    let mimeType = 'video/mp4';
    await createRecorder(stream, mimeType);
  }

  return (
    <Button
      className="reclami-btn"
      value={screenFiles ? screenFiles : null}
      onClick={screenRecording}
    >
      <GoDeviceCameraVideo />
    </Button>
  );
}

export default ScreenRecording;
