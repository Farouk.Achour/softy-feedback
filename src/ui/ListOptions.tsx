import { List } from 'antd';
import ReportBugBtn from '../features/bug/ReportBugBtn';
import RequestFeatureBtn from '../features/feature/RequestFeatureBtn';
import GeneralFeedbackBtn from '../features/feedback/GeneralFeedbackBtn';
// import ContactUsBtn from '../features/contact/ContactUsBtn';

interface Props {
  setPreview: React.Dispatch<React.SetStateAction<string>>;
}

const ListFeedback = ({ setPreview }: Props) => {
  return (
    <>
      <div className="reclami-modal-title">Softy feedback</div>

      <List>
        <ReportBugBtn setPreview={setPreview} />

        <RequestFeatureBtn setPreview={setPreview} />

        <GeneralFeedbackBtn setPreview={setPreview} />

        {/* <ContactUsBtn /> */}
      </List>
    </>
  );
};

export default ListFeedback;
