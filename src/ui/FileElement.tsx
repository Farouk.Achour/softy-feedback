import { HiXMark } from 'react-icons/hi2';
import { removeFile } from '../utils/modifyFormFiles';

function FileElement({ value, files, setFiles }: any) {
  const fileType = value.slice(value.lastIndexOf('.') + 1);
  console.log(fileType);

  function handleRemoveEl() {
    const newFiles = removeFile({ files, value });
    console.log(newFiles);

    setFiles(newFiles);
  }

  return (
    <div className="reclami-file-container">
      {fileType === 'png' && <img className="reclami-item-img" src={value} />}
      {fileType === 'mp4' && (
        <video className="reclami-item-video" src={value} />
      )}
      <div className="reclami-remove-file" onClick={handleRemoveEl}>
        <HiXMark />
      </div>
    </div>
  );
}

export default FileElement;
