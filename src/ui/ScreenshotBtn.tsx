import { Button } from 'antd';
import html2canvas from 'html2canvas';
import { supabaseUrl } from '../services/supabase';
import { uploadFiles } from '../services/apiFiles';
import { useEffect } from 'react';
import { useScreen } from '../contexts/screenshotContext';
import { TbCapture } from 'react-icons/tb';

function ScreenshotBtn({ setScreenFiles, screenFiles, setModalOpen }: any) {
  const { edit, saveScreen, onSaveScreen, onEditAbility } = useScreen();
  console.log(edit, saveScreen);
  // if (!saveScreen) onSaveScreen();
  console.log(edit, saveScreen);
  // const [saveScreen, setSaveScreen] = useState(false);
  // const [edit, setEdit] = useState(false);

  useEffect(
    function () {
      if (saveScreen) {
        html2canvas(document.body).then(async (canvas) => {
          console.log(canvas);
          const dataURL = canvas.toDataURL('image/jpeg').split(';base64,')[1];
          let blobBin = atob(dataURL);
          let array = [];
          for (let i = 0; i < blobBin.length; i++) {
            array.push(blobBin.charCodeAt(i));
          }
          let fileName = `${Math.random()}-screenShot.png`;
          let file = new Blob([new Uint8Array(array)], { type: 'image/png' });
          file = new File([file], fileName, {
            type: 'image/png',
          });
          console.log(file);
          await uploadFiles(file);
          setScreenFiles([
            ...screenFiles,
            `${supabaseUrl}/storage/v1/object/public/files/${fileName}`,
          ]);
        });
        onSaveScreen();
        onEditAbility();
        setModalOpen(true);
      }
    },
    [saveScreen]
  );

  function handleClick() {
    setModalOpen(false);
    onEditAbility();
  }

  return (
    <Button className="reclami-btn" onClick={handleClick}>
      <TbCapture />
    </Button>
  );
}

export default ScreenshotBtn;
