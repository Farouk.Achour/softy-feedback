import { useEffect, useRef, useState } from 'react';
import { HiOutlineXMark } from 'react-icons/hi2';
import { useScreen } from '../contexts/screenshotContext';

function Screenshot() {
  const colors = ['red', 'green', 'black', 'blue'];
  const [startTime, setStartTime] = useState(0);

  const { edit, onEditAbility, onSaveScreen } = useScreen();

  const width = window.innerWidth;
  const height = window.innerHeight;

  const canvasRef: React.MutableRefObject<any> = useRef(null);
  const [context, setContext]: [
    any,
    React.Dispatch<React.SetStateAction<any>>
  ] = useState(null);
  const [drawing, setDrawing] = useState(false);
  const [currentColor, setCurrentColor] = useState(colors[0]);
  const [showToolbar, setShowToolbar] = useState(true);

  //if (!showToolbar) setShowToolbar(true);

  // Initialize the canvas context on component mount

  const startDrawing = () => {
    setDrawing(true);
    context.beginPath();
  };

  const stopDrawing = () => {
    setDrawing(false);
    context.closePath();
  };

  const handleMouseMove = (e: any) => {
    if (!drawing) return;
    const x = e.clientX;
    const y = e.clientY;

    setStartTime((start) => start + 1);

    context.strokeStyle = currentColor;
    context.lineWidth = 5;
    context.lineJoin = 'round';
    context.lineCap = 'round';

    context.lineTo(x, y);
    context.stroke();
  };

  useEffect(() => {
    const canvas = canvasRef.current;
    if (canvas) {
      const ctx = canvas.getContext('2d');
      setContext(ctx);
    }
  }, [startTime, width, height]);

  const captureScreenshot = async () => {
    await onSaveScreen();
    setShowToolbar(true);
  };

  return (
    <>
      {edit && (
        <div className="screenshot">
          <div className="screenshot-cancel-button" onClick={onEditAbility}>
            <HiOutlineXMark />
          </div>
          <canvas
            ref={canvasRef}
            width={width}
            height={height}
            onMouseDown={startDrawing}
            onMouseUp={stopDrawing}
            onMouseMove={handleMouseMove}
          />

          {showToolbar && (
            <div>
              <div className={`screenshot-toolbar`}>
                {colors?.map((color, i) => {
                  return (
                    <div
                      className={`screenshot-toolbar-button ${
                        color === currentColor ? 'active-color' : ''
                      }`}
                      onClick={() => setCurrentColor(color)}
                      key={color}
                    >
                      <div
                        className="screenshot-toolbar-color"
                        key={i}
                        style={{ backgroundColor: color }}
                      ></div>
                    </div>
                  );
                })}
                <div
                  className="screenshot-toolbar-confirm"
                  onClick={() => {
                    setShowToolbar(false);
                    captureScreenshot();
                  }}
                >
                  Confirm
                </div>
              </div>
            </div>
          )}
        </div>
      )}
    </>
  );
}

export default Screenshot;
