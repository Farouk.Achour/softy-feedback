import { Button, Modal } from 'antd';
import React from 'react';
import ListFeedback from './ListOptions';
import FeatureModal from '../features/feature/FeatureModal';
import FormSubmitted from '../features/done/FormSubmitted';
import FeedbackModal from '../features/feedback/FeedbackModal';
import BugModal from '../features/bug/BugModal';

function FeedbackButton() {
  const [modalOpen, setModalOpen] = React.useState<boolean>(false);
  const [preview, setPreview] = React.useState<string>('options');

  return (
    <>
      <Button
        className="reclami-modal-btn"
        type="primary"
        onClick={() => setModalOpen(true)}
      >
        Feedback
      </Button>
      <Modal
        centered
        open={modalOpen}
        onOk={() => setModalOpen(false)}
        onCancel={() => setModalOpen(false)}
        width={400}
        footer={null}
      >
        {preview === 'options' && <ListFeedback setPreview={setPreview} />}
        {preview === 'bug' && (
          <BugModal setPreview={setPreview} setModalOpen={setModalOpen} />
        )}
        {preview === 'feature' && (
          <FeatureModal setPreview={setPreview} setModalOpen={setModalOpen} />
        )}
        {preview === 'feedback' && (
          <FeedbackModal setModalOpen={setModalOpen} setPreview={setPreview} />
        )}
        {preview === 'done' && (
          <FormSubmitted setModalOpen={setModalOpen} setPreview={setPreview} />
        )}
      </Modal>
    </>
  );
}

export default FeedbackButton;
