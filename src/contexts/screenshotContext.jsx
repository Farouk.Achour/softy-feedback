import { createContext, useContext, useMemo, useState } from 'react';

// 1) CREATE A CONTEXT
const ScreenContext = createContext();

function ScreenProvider({ children }) {
  const [saveScreen, setSaveScreen] = useState(false);
  const [edit, setEdit] = useState(false);

  function handleSaveScreen() {
    setSaveScreen(!saveScreen);
  }

  function handleEditAbility() {
    setEdit(!edit);
  }

  const value = useMemo(() => {
    return {
      edit,
      saveScreen,
      onSaveScreen: handleSaveScreen,
      onEditAbility: handleEditAbility,
    };
  }, [edit, saveScreen]);

  return (
    // 2) PROVIDE VALUE TO CHILD COMPONENTS
    <ScreenContext.Provider value={value}>{children}</ScreenContext.Provider>
  );
}

function useScreen() {
  const context = useContext(ScreenContext);
  if (context === undefined)
    throw new Error('PostContext was used outside of the PostProvider');
  return context;
}

export { ScreenProvider, useScreen };
