import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.tsx';
import './index.scss';

export const project_id = document.getElementById('reclami')?.dataset.token;

ReactDOM.createRoot(document.getElementById('reclami')!).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
