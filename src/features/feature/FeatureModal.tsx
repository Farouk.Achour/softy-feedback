import { IoIosArrowBack } from 'react-icons/io';
import FeatureForm from './FeatureForm';

interface Props {
  setPreview: React.Dispatch<React.SetStateAction<string>>;
  setModalOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

function FeatureModal({ setPreview, setModalOpen }: Props) {
  function handleReturnToMainModal() {
    setPreview('options');
  }

  return (
    <>
      <div className="reclami-cover"></div>
      <div className="reclami-header">
        <div
          className="reclami-Return-to-options"
          onClick={handleReturnToMainModal}
        >
          <IoIosArrowBack />
        </div>
        <p>Feature request</p>
      </div>
      <FeatureForm setPreview={setPreview} setModalOpen={setModalOpen} />
    </>
  );
}

export default FeatureModal;
