import { Avatar, Button, List } from 'antd';
import { BsStars } from 'react-icons/bs';

interface Props {
  setPreview: React.Dispatch<React.SetStateAction<string>>;
}

function RequestFeatureBtn({ setPreview }: Props) {
  function handleFeature() {
    setPreview('feature');
  }

  return (
    <Button
      className="reclami-list-item"
      block={true}
      type="text"
      onClick={handleFeature}
    >
      <List.Item>
        <List.Item.Meta
          title={<div className="reclami-list-text">Feature request</div>}
          description={
            <div className="reclami-list-text">Tell us how we can improve</div>
          }
          avatar={<Avatar className="reclami-list-avatar" src={<BsStars />} />}
        />
      </List.Item>
    </Button>
  );
}

export default RequestFeatureBtn;
