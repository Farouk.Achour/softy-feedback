import React, { useState } from 'react';
import { Button, Form, Input } from 'antd';
import ScreenshotBtn from '../../ui/ScreenshotBtn';
import ScreenRecording from '../../ui/ScreenRecording';
import FileElement from '../../ui/FileElement';
import { postFeedback } from '../../services/apiFeedbacks';
import { project_id } from '../../main';

const layout = {
  labelCol: { span: 12 },
  wrapperCol: { span: 24 },
};

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 24 },
};

const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
  },
};

interface FeatureType {
  feedback: {
    type: string;
    rating: any;
    title: string;
    email: string;
    description: string;
    files: any;
    project_id: number;
  };
}

interface Props {
  setPreview: React.Dispatch<React.SetStateAction<string>>;
  setModalOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const FeatureForm = ({ setPreview, setModalOpen }: Props) => {
  let [screenFiles, setScreenFiles]: [
    any,
    React.Dispatch<React.SetStateAction<any>>
  ] = useState([]);

  const onFinish = (
    values: FeatureType,
    { setPreview }: { setPreview: React.Dispatch<React.SetStateAction<string>> }
  ) => {
    values = {
      feedback: {
        type: 'feature',
        rating: undefined,
        title: values.feedback?.title,
        email: values.feedback?.email,
        description: values.feedback?.description,
        files: screenFiles,
        project_id: Number(project_id),
      },
    };
    postFeedback(values);
    setPreview('done');
  };

  return (
    <Form
      {...layout}
      name="nest-messages"
      onFinish={(e) => onFinish(e, { setPreview })}
      validateMessages={validateMessages}
      className="reclami-form"
    >
      <p className="reclami-form-title">Tell us how we can improve</p>
      <Form.Item
        {...formItemLayout}
        name={['feedback', 'email']}
        rules={[
          { type: 'email', required: true, message: 'Your email address' },
        ]}
      >
        <Input
          className="reclami-input-text"
          placeholder="Your email address"
        />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        name={['feedback', 'title']}
        rules={[{ message: 'Give your idea a name' }]}
      >
        <Input
          className="reclami-input-text"
          placeholder="Give your idea a name"
        />
      </Form.Item>
      <Form.Item
        name={['feedback', 'description']}
        rules={[{ message: 'Tell us more, including the problem it solves' }]}
      >
        <Input.TextArea
          className="reclami-text-area"
          placeholder="Tell us more, including the problem it solves"
        />
      </Form.Item>

      <div className="reclami-files featureFilesBtn">
        <Button className="reclami-btn-wrapper">
          <ScreenRecording
            setModalOpen={setModalOpen}
            screenFiles={screenFiles}
            setScreenFiles={setScreenFiles}
          />
          <ScreenshotBtn
            setScreenFiles={setScreenFiles}
            screenFiles={screenFiles}
            setModalOpen={setModalOpen}
          />
        </Button>
      </div>

      <div className="reclami-files-wrapper">
        {screenFiles.map((value: any, i: number) => {
          console.log(value);
          return (
            <FileElement
              key={i}
              files={screenFiles}
              value={value}
              setFiles={setScreenFiles}
            />
          );
        })}
      </div>

      <Form.Item>
        <Button className="reclami-form-btn" type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

export default FeatureForm;
