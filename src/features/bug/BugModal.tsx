import { IoIosArrowBack } from 'react-icons/io';
import BugForm from './BugForm';

interface Props {
  setPreview: React.Dispatch<React.SetStateAction<string>>;
  setModalOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

function FeatureModal({ setPreview, setModalOpen }: Props) {
  function handleReturnToMainModal() {
    setPreview('options');
  }

  return (
    <>
      <div className="reclami-cover"></div>
      <div className="reclami-header">
        <div
          className="reclami-Return-to-options"
          onClick={handleReturnToMainModal}
        >
          <IoIosArrowBack />
        </div>
        <p>Report a bug</p>
      </div>
      <BugForm setPreview={setPreview} setModalOpen={setModalOpen} />
    </>
  );
}

export default FeatureModal;
