import { Avatar, Button, List } from 'antd';
import { PiBugDroidDuotone } from 'react-icons/pi';

interface Props {
  setPreview: React.Dispatch<React.SetStateAction<string>>;
}

function ReportBugBtn({ setPreview }: Props) {
  function handleBug() {
    setPreview('bug');
  }

  return (
    <Button
      className="reclami-list-item"
      block={true}
      type="text"
      onClick={handleBug}
    >
      <List.Item>
        <List.Item.Meta
          title={<div className="reclami-list-text">Report a bug</div>}
          description={
            <div className="reclami-list-text">Let us know what's broken</div>
          }
          avatar={
            <Avatar
              className="reclami-list-avatar"
              src={<PiBugDroidDuotone />}
            />
          }
        />
      </List.Item>
    </Button>
  );
}

export default ReportBugBtn;
