import { IoCheckmarkCircle } from 'react-icons/io5';

interface Props {
  setModalOpen: React.Dispatch<React.SetStateAction<boolean>>;
  setPreview: React.Dispatch<React.SetStateAction<string>>;
}

function FormSubmitted({ setModalOpen, setPreview }: Props) {
  setTimeout(() => {
    setModalOpen(false);
    setPreview('options');
  }, 3000);

  return (
    <div className="reclami-form-done">
      <IoCheckmarkCircle className="reclami-icon" />
      <p>Thank you 💪</p>
    </div>
  );
}

export default FormSubmitted;
