import { IoIosArrowBack } from 'react-icons/io';
import FeedbackForm from './FeedbackForm';

interface Props {
  setPreview: React.Dispatch<React.SetStateAction<string>>;
  setModalOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

function FeedbackModal({ setPreview, setModalOpen }: Props) {
  function handleReturnToMainModal() {
    setPreview('options');
  }

  return (
    <>
      <div className="reclami-cover"></div>
      <div className="reclami-header">
        <div
          className="reclami-Return-to-options"
          onClick={handleReturnToMainModal}
        >
          <IoIosArrowBack />
        </div>
        <p>Feature request</p>
      </div>
      <FeedbackForm setPreview={setPreview} setModalOpen={setModalOpen} />
    </>
  );
}

export default FeedbackModal;
