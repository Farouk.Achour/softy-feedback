import { Avatar, Button, List } from 'antd';
import { IoDocumentText } from 'react-icons/io5';

interface Props {
  setPreview: React.Dispatch<React.SetStateAction<string>>;
}

function GeneralFeedbackBtn({ setPreview }: Props) {
  function handleFeedback() {
    setPreview('feedback');
  }

  return (
    <Button
      className="reclami-list-item"
      block={true}
      type="text"
      onClick={handleFeedback}
    >
      <List.Item>
        <List.Item.Meta
          title={<div className="reclami-list-text">General feedback</div>}
          description={
            <div className="reclami-list-text">Provide general feedback</div>
          }
          avatar={
            <Avatar className="reclami-list-avatar" src={<IoDocumentText />} />
          }
        />
      </List.Item>
    </Button>
  );
}

export default GeneralFeedbackBtn;
