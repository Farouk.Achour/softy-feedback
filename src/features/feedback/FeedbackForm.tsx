import React, { lazy, useState } from 'react';
import { Button, Form, Input, Rate } from 'antd';
import { postFeedback } from '../../services/apiFeedbacks';
import { uploadFiles } from '../../services/apiFiles';
import { supabaseUrl } from '../../services/supabase';
import ScreenshotBtn from '../../ui/ScreenshotBtn';
import ScreenRecording from '../../ui/ScreenRecording';
import { project_id } from '../../main';
const FileElement = lazy(() => import('../../ui/FileElement'));

const layout = {
  labelCol: { span: 12 },
  wrapperCol: { span: 24 },
};

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 24 },
};

const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
  },
};

interface FeedbackType {
  feedback: {
    type: string;
    rating: number;
    title: any;
    email: string;
    description: string;
    files: any;
    project_id: number;
  };
}

interface Props {
  setPreview: React.Dispatch<React.SetStateAction<string>>;
  setModalOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const FeedbackForm = ({ setPreview, setModalOpen }: Props) => {
  let [screenFiles, setScreenFiles]: [
    any,
    React.Dispatch<React.SetStateAction<any>>
  ] = useState([]);
  console.log(screenFiles);

  const onFinish = (
    values: FeedbackType,
    { setPreview }: { setPreview: React.Dispatch<React.SetStateAction<string>> }
  ) => {
    let filesList = [];
    if (values.feedback.files) {
      values.feedback.files.fileList.map((file: any) => uploadFiles(file));
      filesList = values.feedback.files.fileList.map(
        (file: any) =>
          `${supabaseUrl}/storage/v1/object/public/files/${file.name}`
      );
    }

    values = {
      feedback: {
        type: 'feedback',
        rating: values.feedback?.rating,
        title: undefined,
        description: values.feedback?.description,
        email: values.feedback?.email,
        files: [...filesList, ...screenFiles],
        project_id: Number(project_id),
      },
    };
    postFeedback(values);
    setPreview('done');
  };

  return (
    <Form
      {...layout}
      name="nest-messages"
      onFinish={(e) => onFinish(e, { setPreview })}
      validateMessages={validateMessages}
      className="reclami-form"
    >
      <p className="reclami-form-title">How would you rate your experience</p>

      <Form.Item {...formItemLayout} name={['feedback', 'rating']}>
        <Rate className="reclami-rating" />
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        name={['feedback', 'email']}
        rules={[
          { type: 'email', required: true, message: 'Your email address' },
        ]}
      >
        <Input className="reclami-input-text" placeholder="Your email address" />
      </Form.Item>
      <Form.Item
        name={['feedback', 'description']}
        rules={[{ message: 'Leave us your comment' }]}
      >
        <Input.TextArea
          className="reclami-text-area"
          placeholder="Leave us your comment"
        />
      </Form.Item>
      <div className="reclami-files reclami-featureFilesBtn">
        <Button className="reclami-btn-wrapper">
          <ScreenRecording
            setModalOpen={setModalOpen}
            screenFiles={screenFiles}
            setScreenFiles={setScreenFiles}
          />
          <ScreenshotBtn
            setScreenFiles={setScreenFiles}
            screenFiles={screenFiles}
            setModalOpen={setModalOpen}
          />
          {/* <Form.Item {...formItemLayout} name={['feedback', 'files']}>
            <Upload showUploadList={false} {...props}>
              <Button>
                <ImAttachment />
              </Button>
            </Upload>
          </Form.Item> */}
        </Button>
      </div>

      <div className="reclami-files-wrapper">
        {screenFiles.map((value: any, i: number) => {
          console.log(value);
          return (
            <FileElement
              key={i}
              files={screenFiles}
              value={value}
              setFiles={setScreenFiles}
            />
          );
        })}
      </div>

      <Form.Item>
        <Button className="reclami-form-btn" type="primary" htmlType="submit">
          Send feedback
        </Button>
      </Form.Item>
    </Form>
  );
};

export default FeedbackForm;
