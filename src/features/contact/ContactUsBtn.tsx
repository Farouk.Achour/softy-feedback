import { Avatar, Button, List } from 'antd';
import { RiQuestionMark } from 'react-icons/ri';

function ContactUsBtn() {
  return (
    <Button disabled className="reclami-list-item" block={true} type="text">
      <List.Item>
        <List.Item.Meta
          title={<div className="reclami-list-text">Contact us</div>}
          description={
            <div className="reclami-list-text">Get in touch with us</div>
          }
          avatar={
            <Avatar className="reclami-list-avatar" src={<RiQuestionMark />} />
          }
        />
      </List.Item>
    </Button>
  );
}

export default ContactUsBtn;
