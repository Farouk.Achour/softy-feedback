import supabase from './supabase';

export async function uploadFiles(file: any) {
  console.log(file);
  const { data, error } = await supabase.storage
    .from('files')
    .upload(`${file.name}`, file.originFileObj ? file.originFileObj : file);

  if (error) console.log(error);

  console.log(data);
  return data;
}
