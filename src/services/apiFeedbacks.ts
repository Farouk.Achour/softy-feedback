import supabase from './supabase.js';

interface Props {
  feedback: {
    type: string;
    rating: number;
    title: string;
    email: string;
    description: string;
    files: any;
    project_id: any;
  };
}

export async function postFeedback({ feedback }: Props) {
  console.log(feedback);

  const { data, error } = await supabase
    .from('feedbacks')
    .insert(feedback)
    .select();

  if (error) console.log(error);

  console.log(data);
  return data;
}
